#!/bin/bash

for METHOD in LinearRegression Ridge Lasso ElasticNet RandomForestRegressor \
        GradientBoostingRegressor SGDRegressor KNeighborsRegressor SVR \
        DecisionTreeRegressor AdaBoostRegressor MLPRegressor ExtraTreesRegressor \
        XGBRegressor LGBMRegressor KernelRidge PolynomialRegression DummyRegressor
do
    echo $METHOD
done