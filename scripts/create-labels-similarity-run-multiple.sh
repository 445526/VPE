#!/bin/bash
for METHOD in rmse r2 mae median_absolute_error pearson spearman euclidean SID SAM entropy NormXCorr chebyshev
do
    for PREPROCESS in none RobustScaler StandardScaler MinMaxScaler
    do
        for FEATURE_SELECT in none SelectKBest PCA SelectPercentile VarianceThreshold
        do
            for PARAMETER in Car Cab Cw
            do
                qsub -q elixircz@elixir-pbs.elixir-czech.cz -l walltime=24:00:00 -l select=1:mem=20gb:ncpus=1 \
                -N LabelAssigner-gridsearch-METHOD=$METHOD-PREPROCESS=$PREPROCESS-FEATURE_SELECT=$FEATURE_SELECT-PARAMETER=$PARAMETER \
                -v METHOD="$METHOD",PREPROCESS="$PREPROCESS",FEATURE_SELECT="$FEATURE_SELECT",PARAMETER="$PARAMETER" \
                create-labels.sh
            done
        done
    done
done