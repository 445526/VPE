#!/bin/bash

module add python/3.8.0-gcc
source /storage/brno3-cerit/home/tslaninakova/enviro/vpe-env/bin/activate
cd /storage/brno3-cerit/home/tslaninakova/enviro-cleanup/VPE/scripts

python create-labels.py --method=$METHOD --preprocess=$PREPROCESS --parameter=$PARAMETER \
--feature-select=$FEATURE_SELECT \
2>&1 | tee METHOD=$METHOD-PREPROCESS=$PREPROCESS-FEATURE_SELECT=$FEATURE_SELECT.log