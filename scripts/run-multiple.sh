#!/bin/bash

for PREPROCESS in True False #True
do
    for MODEL in resnet18 resnet34 resnet50 resnet101 resnet152
    do
        for PATCH_SIZE in 1 3 5 10 15
        do
            for LR in 0.001 0.01 0.1
            do
            # qsub -q gpu -l walltime=24:00:00 -l select=1:mem=10gb:ngpus=1:ncpus=1:cluster=adan \
            qsub -q elixircz@elixir-pbs.elixir-czech.cz -l walltime=24:00:00 -l select=1:mem=10gb:ncpus=1 \
            -N train_predict_regression-CPU-PRETRAINED-$PRETRAINED-MODEL-$MODEL-PATCH_SIZE-$PATCH_SIZE-LR=$LR \
            -v LR="$LR",PRETRAINED="$PRETRAINED",MODEL="$MODEL",PATCH_SIZE="$PATCH_SIZE" \
            run-train-predict-regression-resnet.sh
            done
        done
    done
done