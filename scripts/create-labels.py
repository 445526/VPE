import logging
from argparse import ArgumentParser

import pandas as pd
import numpy as np
import time
from sklearn.model_selection import GridSearchCV, PredefinedSplit

from veg_estimator.LabelAssigner import LabelAssigner, SimilarityLabelAssigner
from veg_estimator.DataLoader import DataLoader
from veg_estimator.utils import create_dir, file_exists, get_logger_config

logging.basicConfig(level=logging.INFO, format=get_logger_config())
LOG = logging.getLogger(__name__)

# Leverage Label assigner to create the best labels given LUT and a portion of valid data

# Hyperparameters:
# - model hyperparameters (grid search)
# - data hyperparameters (data normalization, feature selection)]
# - brute-force sim-s. instead of using ML
# Uses up 20% of the valid data, one half to tune the hyps with, the other for testing
    # saved in results/artifacts/label-create/train.csv, valid.csv, test.csv
    # - metrics: mae | mse | rmse | rrmse | r2
# Output:
#   - raw: as mulitple csvs in results/raw for each ML/Sim. s. setup and the metrics: name | params | feature_select | preprocess | mae | mse | rmse | rrmse | r2
#   - aggregate: as one csv in results/aggregated: name | best_params | feature_select | preprocess | mae | mse | rmse | rrmse | r2
def load_data():
    loader = DataLoader()
    lut, lut_params = loader.load_LUT_data()
    valid_data, valid_params = loader.load_valid_data()
    LOG.info(
        f'Loaded data: valid: {valid_data.shape}, valid labels: {valid_params.shape},'
        f'lut: {lut.shape}, lut params: {lut_params.shape}'
    )
    return lut, lut_params, valid_data, valid_params


def join_data_on_common_columns(lut, lut_params, valid_data, valid_params):
    common_columns = lut.columns.intersection(valid_data.columns)
    LOG.info(f'Common columns: {common_columns}')
    lut = lut.loc[:, common_columns]
    valid_data = valid_data.loc[:, common_columns]
    return lut, lut_params, valid_data, valid_params


def prepare_for_train(lut, lut_params, valid_data, valid_params, param):
    from sklearn.model_selection import train_test_split
    _, X_test, _, y_test = train_test_split(
        valid_data, valid_params[param], test_size=0.2, random_state=2023
    )

    X_valid, X_test, y_valid, y_test = train_test_split(
        X_test, y_test, test_size=0.5, random_state=2023
    )
    LOG.info(
        f'Split data into valid: {X_valid.shape}, {y_valid.shape},'
        f'and test: {X_test.shape}, {y_test.shape}.'
    )
    X_train = lut
    y_train = lut_params[param]

    X = pd.concat([X_train, X_valid]).reset_index().drop('index', axis=1)
    y = pd.concat([y_train, y_valid]).reset_index().drop('index', axis=1).values.ravel()

    split_index = [-1 if x in lut.index else 0 for x in X.index]
    LOG.info(
        f'Split index ([-10:]): {split_index[-10:]},\n'
        f'Unique values in split index: {np.unique(split_index, return_counts=True)}\n'
        f'Shape of data going into training: X: {X.shape}, y: {y.shape}'
    )
    # TODO: Save split data
    return (X, y, X_train, y_train, X_valid, y_valid, X_test, y_test, split_index)


def train(
    model,
    scaler,
    feature_select,
    X,
    y,
    X_train,
    y_train,
    X_valid,
    y_valid,
    X_test,
    y_test,
    split_index
):
    if model in [
        'rmse', 'r2', 'mae', 'median_absolute_error', 'pearson', 'spearman', 'euclidean',
        'SID', 'SAM', 'entropy', 'NormXCorr', 'chebyshev'
    ]:
        la = SimilarityLabelAssigner(model, scaler, feature_select)
    else:
        la = LabelAssigner(model, scaler, feature_select)
    LOG.info(
        f'Running grid search on model={model} with scaler={scaler} and feature_select={feature_select}.'
    )
    LOG.info(f'Running grid search with {la.grid_params}.')

    # assures that we only use the valid points assigned at the end of `X`
    # i.e., no real cross-validation is done
    pds = PredefinedSplit(test_fold=split_index)

    s = time.time()
    grid_search = GridSearchCV(
        estimator=la.pipeline,
        param_grid=la.grid_params,
        cv=pds,
        n_jobs=-1,
        scoring='neg_root_mean_squared_error',
        verbose=1
    )
    grid_search.fit(X, y)
    LOG.info(f'Fitted the pipeline in {time.time() - s}')

    #X_valid = grid_search.transform(X_valid)
    #X_test = grid_search.transform(X_test)

    train_metrics = la.evaluate(grid_search.predict(X_train), y_train)
    valid_metrics = la.evaluate(grid_search.predict(X_valid), y_valid)
    test_metrics = la.evaluate(grid_search.predict(X_test), y_test)

    return train_metrics, valid_metrics, test_metrics, grid_search

def save_results(
    model,
    scaler,
    feature_select,
    param,
    train_metrics,
    valid_metrics,
    test_metrics,
    best_params,
    all_results,
    output_path
):
    create_dir(f'{output_path}/aggregate')
    create_dir(f'{output_path}/raw')
    pd.DataFrame.from_dict(all_results).to_csv(
        f'{output_path}/raw/model-{model}--scaler-{scaler}--fs-{feature_select}--{param}.csv',
        index=False
    )

    out_path = (
        f'{output_path}/aggregate/create-labels-results.csv'
    )
    
    include_header = False if file_exists(out_path) else True
    pd.DataFrame({
        'model': [model],
        'scaler': [scaler],
        'feature_select': [feature_select],
        'param': [param],
        'best_params': [best_params],
        'train_mae': [train_metrics['mae']],
        'train_mse': [train_metrics['mse']],
        'train_rmse': [train_metrics['rmse']],
        'train_rrmse': [train_metrics['rrmse']],
        'train_r2': [train_metrics['r2']],
        'valid_mae': [valid_metrics['mae']],
        'valid_mse': [valid_metrics['mse']],
        'valid_rmse': [valid_metrics['rmse']],
        'valid_rrmse': [valid_metrics['rrmse']],
        'valid_r2': [valid_metrics['r2']],
        'test_mae': [test_metrics['mae']],
        'test_mse': [test_metrics['mse']],
        'test_rmse': [test_metrics['rmse']],
        'test_rrmse': [test_metrics['rrmse']],
        'test_r2': [test_metrics['r2']]
    }).to_csv(out_path, mode='a', header=include_header, index=False)

    LOG.info(f'Saved raw results to {out_path}/raw and {output_path}/aggregate.')

# TODOs: create sim search methods, create feature selection methods
if __name__ == '__main__':
    argparse = ArgumentParser()
    argparse.add_argument(
        '-m', '--method', type=str, help='Which model or sim. search method to use'
        )
    argparse.add_argument(
        '-p', '--preprocess', type=str, default=None, help='Which preprocessing method to use'
        )
    argparse.add_argument(
        '-f',
        '--feature-select',
        type=str,
        default=None,
        help='Which feature selection method to use'
        )
    argparse.add_argument('--parameter', type=str, default='Cab', help='Which parameter to use')
    argparse.add_argument(
        '--output-path', type=str, default='../results/', help='path to the output folder'
        )
    args = argparse.parse_args()

    assert args.method in [
        'LinearRegression', 'Ridge', 'Lasso', 'ElasticNet', 'RandomForestRegressor',
        'GradientBoostingRegressor', 'SGDRegressor', 'KNeighborsRegressor', 'SVR',
        'DecisionTreeRegressor', 'AdaBoostRegressor', 'MLPRegressor', 'ExtraTreesRegressor',
        'XGBRegressor', 'LGBMRegressor', 'KernelRidge', 'PolynomialRegression', 'DummyRegressor',
        'rmse', 'r2', 'mae', 'median_absolute_error', 'pearson', 'spearman', 'euclidean', 'SID',
        'SAM', 'entropy', 'NormXCorr', 'chebyshev'
    ], 'Method must be known (see source code).'
    assert args.preprocess in [None, 'none', 'RobustScaler', 'StandardScaler', 'MinMaxScaler'], \
        "preprocess must be one of None, 'none', 'RobustScaler', 'StandardScaler', 'MinMaxScaler'"
    assert args.feature_select in \
        [None, 'none', 'SelectKBest', 'SelectPercentile', 'VarianceThreshold', 'PCA'], \
        (
            "feature_select must be one of 'none', 'SelectKBest', 'SelectPercentile',"
            " 'VarianceThreshold', 'PCA'"
        )
    assert args.parameter in ['Cab', 'Car', 'Cw'], 'param must be one of Cab, Car, Cw'

    lut, lut_params, valid_data, valid_params = load_data()
    lut, lut_params, valid_data, valid_params = join_data_on_common_columns(
        lut, lut_params, valid_data, valid_params
    )
    (X, y, X_train, y_train, X_valid, y_valid, X_test, y_test, split_index) = prepare_for_train(
        lut, lut_params, valid_data, valid_params, args.parameter
    )
    train_metrics, valid_metrics, test_metrics, grid_search = train(
        args.method,
        args.preprocess,
        args.feature_select,
        X,
        y,
        X_train,
        y_train,
        X_valid,
        y_valid,
        X_test,
        y_test,
        split_index
    )

    save_results(
        args.method,
        args.preprocess,
        args.feature_select,
        args.parameter,
        train_metrics,
        valid_metrics,
        test_metrics,
        grid_search.best_params_,
        grid_search.cv_results_,
        args.output_path
    )