#!/bin/bash

# Run these with more CPUs -- for METHOD in ElasticNet ExtraTreesRegressor LGBMRegressor

# Run this one with more RAM (more than 40gb): for METHOD in SGDRegressor MLPRegressor

# Run these with one CPU
#for METHOD in LinearRegression Ridge Lasso RandomForestRegressor \
#        GradientBoostingRegressor KNeighborsRegressor SVR \
#        DecisionTreeRegressor AdaBoostRegressor \
#        XGBRegressor KernelRidge PolynomialRegression DummyRegressor
for METHOD in ElasticNet ExtraTreesRegressor LGBMRegressor
do
    for PREPROCESS in none RobustScaler StandardScaler MinMaxScaler
    do
        for FEATURE_SELECT in none SelectKBest PCA SelectPercentile VarianceThreshold
        do
            for PARAMETER in Car Cab Cw
            do
                qsub -q elixircz@elixir-pbs.elixir-czech.cz -l walltime=24:00:00 -l select=1:mem=40gb:ncpus=10 \
                -N LabelAssigner-gridsearch-METHOD=$METHOD-PREPROCESS=$PREPROCESS-FEATURE_SELECT=$FEATURE_SELECT-PARAMETER=$PARAMETER \
                -v METHOD="$METHOD",PREPROCESS="$PREPROCESS",FEATURE_SELECT="$FEATURE_SELECT",PARAMETER="$PARAMETER" \
                create-labels.sh
            done
        done
    done
done