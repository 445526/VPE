numpy==1.24.2
pandas==1.5.3
matplotlib==3.7.1
seaborn==0.12.2
plotly==5.14.1
scikit-learn==1.2.2
jupyterlab==3.6.2
tqdm==4.65.0
# for experiment metadata:
# -> get name of running notebook
ipynbname==2021.3.2
# -> get current sha of git repo
gitpython==3.1.31
xgboost==1.7.6
lightgbm==4.0.0
scipy==1.9.1