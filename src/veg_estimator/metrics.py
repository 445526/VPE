import math
import numpy as np


def rmse(y1: float, y2: float) -> float:
    """ Root Mean Squared Error.

    Parameters
    ----------
    y1 : float
        The true value
    y2 : float
        The predicted value

    Returns
    -------
        float
    """
    return math.sqrt(np.square(np.subtract(y1, y2)).mean())


def rrmse(true: float, pred: float) -> float:
    """ Root Relative Mean Squared Error

    Parameters
    ----------
    true : float
        The true value
    pred : float
        The predicted value

    Returns
    -------
        float
    """
    num = np.sum(np.square(true - pred))
    den = np.sum(np.square(pred))
    squared_error = num/den
    rrmse_loss = np.sqrt(squared_error)
    return rrmse_loss
