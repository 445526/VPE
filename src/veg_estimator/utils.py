import pickle
from datetime import datetime
from pathlib import Path
import git
import ipynbname


def load_pickle(path: str) -> object:
    """ Load a pickle file.

    Parameters
    ----------
    path : str
        The path to the pickle file

    Returns
    -------
    loaded object : object

    """
    with open(path, 'rb') as f:
        data = pickle.load(f)
    return data

def load_json(path: str) -> dict:
    """ Load a json file.

    Parameters
    ----------
    path : str
        The path to the json file

    Returns
    -------
    loaded object : dict

    """
    import json
    with open(path, 'r') as f:
        data = json.load(f)
    return data


def create_dir(directory: str) -> None:
    """ Creates a directory if it does not exist.

    Parameters
    ----------
    directory : str
        Path to the directory.
    """
    Path(directory).mkdir(parents=True, exist_ok=True)


def file_exists(filename: str) -> bool:
    """ Checks if a file exists."""
    return Path.is_file(Path(filename))


def get_current_datetime() -> str:
    """
    Formats current datetime into a string.

    Returns
    ----------
    str
        Created datetime.
    """
    return datetime.now().strftime('%Y-%m-%d--%H-%M-%S')


def get_git_revision_hash() -> str:
    """ Returns the git revision hash.

    Returns
    ----------
    str
        The git revision hash.
    """
    return git.Repo(search_parent_directories=True).head.object.hexsha


def get_current_notebook_name() -> str:
    """ Returns the name of the current notebook.

    Returns
    ----------
    str
        The name of the current notebook.
    """
    return str(ipynbname.path())


def save_json(path: str, data: dict) -> None:
    """ Saves a json file.

    Parameters
    ----------
    path : str
        Path to the directory.
    data : dict
        The data to save.
    """
    import json
    with open(path, 'w') as f:
        json.dump(data, f, indent=4)


def get_logger_config() -> str:
    return '[%(asctime)s][%(levelname)-5.5s][%(name)-.20s] %(message)s'
