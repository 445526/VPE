
def S2_band_wavelength_mapping():
    # source: S2_Lanzhot_20160831_DEM.hdr
    make_consistent_with_lut_form = lambda x: round(x*1000, 2)
    wavelengths = {
        'S2_B02': make_consistent_with_lut_form(0.49654),
        'S2_B03': make_consistent_with_lut_form(0.56001),
        'S2_B04': make_consistent_with_lut_form(0.66445),
        'S2_B05': make_consistent_with_lut_form(0.70389),
        'S2_B06': make_consistent_with_lut_form(0.74022),
        'S2_B07': make_consistent_with_lut_form(0.78247),
        'S2_B08A': make_consistent_with_lut_form(0.8648),
        'S2_B11': make_consistent_with_lut_form(1.6137),
        'S2_B12': make_consistent_with_lut_form(2.2024)
    }
    return wavelengths