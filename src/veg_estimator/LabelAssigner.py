import abc
import math
import numpy as np
import pandas as pd
from scipy.stats import entropy
from pysptools.distance import SID, SAM, NormXCorr, chebyshev
from scipy.stats import pearsonr, spearmanr
from sklearn.metrics import r2_score, mean_absolute_error, median_absolute_error
from sklearn import metrics
from sklearn.base import BaseEstimator, RegressorMixin
from sklearn.pipeline import make_pipeline, Pipeline
from veg_estimator.utils import file_exists, load_json


class SimilarityLabelRegressor(BaseEstimator, RegressorMixin):
    
    def __init__(self, metric):
        self.metric, bigger_better = self._get_metric_fn(metric)
        self.bigger_better = bigger_better
        self.X = None
    
    def fit(self, X, y):
        """We want to remember the training data, so we can use it later for similarity."""
        self.X = X.values if isinstance(X, pd.DataFrame) else X
        self.y = y

    def predict(self, X_test):
        """Predict the labels for the provided data.
        Parameters
        ----------
        X : array-like of shape (n_samples, n_features)
            The input samples.
        Returns
        -------
        y : ndarray of shape (n_samples,)
            Returns predicted values.
        """
        predicted = []
        X_test = X_test.values if isinstance(X_test, pd.DataFrame) else X_test
        for test_row in X_test:
            curr = 100000
            sim_id = -1
            for i, r in enumerate(self.X):
                val = self.metric(test_row, r)
                if self.bigger_better and val > curr or not self.bigger_better and val < curr:
                    sim_id = i
                    curr = val
            predicted.append(self.y[sim_id])
        return predicted
    
    def _get_metric_fn(self, metric):
        def rmse(y1, y2):
            return math.sqrt(np.square(np.subtract(y1, y2)).mean())

        def get_euclidean(a, b):
            return np.linalg.norm(a-b)

        if metric == 'rmse' or metric == rmse:
            return rmse, False
        elif metric == 'r2' or metric == r2_score:
            return r2_score, True
        elif metric == 'mae' or metric == mean_absolute_error:
            return mean_absolute_error, False
        elif metric == 'median_absolute_error' or metric == median_absolute_error:
            return median_absolute_error, False
        elif metric == 'pearson' or metric == pearsonr:
            return pearsonr, True
        elif metric == 'spearman' or metric == spearmanr:
            return spearmanr, True
        elif metric == 'euclidean' or metric == get_euclidean:
            return get_euclidean, False
        elif metric == 'SID' or metric == SID:
            return SID, False
        elif metric == 'SAM' or metric == SAM:
            return SAM, True
        elif metric == 'entropy' or metric == entropy:
            return entropy, False
        elif metric == 'NormXCorr' or metric == NormXCorr:
            return NormXCorr, True
        elif metric == 'chebyshev' or metric == chebyshev:
            return chebyshev, False
        else:
            raise ValueError(f'Unknown metric: {metric}')


class BaseLabelAssigner():
    __metaclass__ = abc.ABCMeta

    def __init__(self, model, scaler, feature_select, configs_path='../configs'):
        self.model = self._get_model_fn(model)
        self.scaler = self._get_scaler_fn(scaler)
        self.feature_select = self._get_feature_select_fn(feature_select)
        self.grid_params = self._get_grid_params(model, configs_path, key_prepend='model')
        
        if self.feature_select is not None:
            self.feature_select_grid_params = self._get_grid_params(
                feature_select, f'{configs_path}/feature-select', key_prepend='selector'
            )
            self.feature_select_grid_params = self._make_callable(self.feature_select_grid_params)
            self.grid_params.update(self.feature_select_grid_params)
        
        self.pipeline = self._build_pipeline()

    @abc.abstractmethod
    def _get_model_fn(self, model):
        pass

    def _get_scaler_fn(self, scaler):
        if scaler == 'none':
            return None
        elif scaler is None:
            return None
        elif scaler == 'RobustScaler':
            from sklearn.preprocessing import RobustScaler
            return RobustScaler()
        elif scaler == 'StandardScaler':
            from sklearn.preprocessing import StandardScaler
            return StandardScaler()
        elif scaler == 'MinMaxScaler':
            from sklearn.preprocessing import MinMaxScaler
            return MinMaxScaler()
        else:
            raise ValueError(f'Unknown scaler: {scaler}')

    def _get_grid_params(self, model, configs_path, key_prepend=''):
        path = f'{configs_path}/{model}-grid-params.json'
        if file_exists(path):
            loaded_config = load_json(path)
            if key_prepend != '':
                loaded_config = {
                    f'{key_prepend}__{k}':v for k,v in loaded_config.items()
                }
        else:
            loaded_config = {}
        return loaded_config


    def _get_feature_select_fn(self, feature_select):
        if feature_select == 'none':
            return None
        elif feature_select is None:
            return None
        elif feature_select == 'SelectKBest':
            from sklearn.feature_selection import SelectKBest
            return SelectKBest()
        elif feature_select == 'SelectPercentile':
            from sklearn.feature_selection import SelectPercentile
            return SelectPercentile()
        elif feature_select == 'VarianceThreshold':
            from sklearn.feature_selection import VarianceThreshold
            return VarianceThreshold()
        elif feature_select == 'PCA':
            from sklearn.decomposition import PCA
            return PCA()
        else:
            raise ValueError(f'Unknown feature_select: {feature_select}')

    def _make_callable(self, grid_params):
        def correct(v):
            if v == 'f-regression':
                from sklearn.feature_selection import f_regression
                return f_regression
            elif v == 'mutual_info-regression':
                from sklearn.feature_selection import mutual_info_regression
                return mutual_info_regression
            else:
                raise ValueError(f'Unknown score_func: {v}')

        for k,v in grid_params.items():
            if 'score_func' in k:
                if isinstance(v, list):
                    new_values = []
                    for v_ in v:
                        new_values.append(correct(v_))
                    grid_params[k] = new_values
                else:
                    grid_params[k] = correct(v)
        return grid_params

    def _build_pipeline(self):
        if self.feature_select is not None:
            if self.scaler is not None:
                return Pipeline([
                    ('scaler', self.scaler),
                    ('selector', self.feature_select),
                    ('model', self.model)
                ])
            else:
                return Pipeline([
                    ('selector', self.feature_select),
                    ('model', self.model)
                ])
        else:
            if self.scaler is not None:
                return Pipeline([
                    ('scaler', self.scaler),
                    ('model', self.model)
                ])
            else:   
                return Pipeline([
                    ('model', self.model)
                ])

    @staticmethod
    def evaluate(predicted, true):
        mae = metrics.mean_absolute_error(true, predicted)
        mse = metrics.mean_squared_error(true, predicted)
        rmse = np.sqrt(metrics.mean_squared_error(true, predicted))
        # source: http://dx.doi.org/10.1016/j.rser.2015.11.058
        rrmse = rmse / np.mean(true)
        # rrmse based on min-max in the training (+validation) set
        r2_square = metrics.r2_score(true, predicted)
        
        return {
            'mae': mae,
            'mse': mse,
            'rmse': rmse,
            'rrmse': rrmse,
            'r2': r2_square
        }


class LabelAssigner(BaseLabelAssigner):
    
    def __init__(self, model, scaler, feature_select, configs_path='../configs'):
        super().__init__(model, scaler, feature_select, configs_path)

    def _get_model_fn(self, model):
        if model == 'LinearRegression':
            from sklearn.linear_model import LinearRegression
            return LinearRegression()
        elif model == 'Ridge':
            from sklearn.linear_model import Ridge
            return Ridge()
        elif model == 'Lasso':
            from sklearn.linear_model import Lasso
            return Lasso()
        elif model == 'ElasticNet':
            from sklearn.linear_model import ElasticNet
            return ElasticNet()
        elif model == 'RandomForestRegressor':
            from sklearn.ensemble import RandomForestRegressor
            return RandomForestRegressor()
        elif model == 'GradientBoostingRegressor':
            from sklearn.ensemble import GradientBoostingRegressor
            return GradientBoostingRegressor()
        elif model == 'SGDRegressor':
            from sklearn.linear_model import SGDRegressor
            return SGDRegressor()
        elif model == 'KNeighborsRegressor':
            from sklearn.neighbors import KNeighborsRegressor
            return KNeighborsRegressor()
        elif model == 'SVR':
            from sklearn.svm import SVR
            return SVR()
        elif model == 'DecisionTreeRegressor':
            from sklearn.tree import DecisionTreeRegressor
            return DecisionTreeRegressor()
        elif model == 'AdaBoostRegressor':
            from sklearn.ensemble import AdaBoostRegressor
            return AdaBoostRegressor()
        elif model == 'MLPRegressor':
            from sklearn.neural_network import MLPRegressor
            return MLPRegressor()
        elif model == 'ExtraTreesRegressor':
            from sklearn.ensemble import ExtraTreesRegressor
            return ExtraTreesRegressor()
        elif model == 'XGBRegressor':
            from xgboost import XGBRegressor
            return XGBRegressor()
        elif model == 'LGBMRegressor':
            from lightgbm import LGBMRegressor
            return LGBMRegressor()
        elif model == 'KernelRidge':
            from sklearn.kernel_ridge import KernelRidge
            return KernelRidge()
        elif model == 'PolynomialRegression':
            from sklearn.preprocessing import PolynomialFeatures
            from sklearn.linear_model import LinearRegression
            def PolynomialRegression(degree=2, **kwargs):
                return make_pipeline(PolynomialFeatures(degree), LinearRegression(**kwargs))
            return PolynomialRegression()
        elif model == 'DummyRegressor':
            from sklearn.dummy import DummyRegressor
            return DummyRegressor()
        else:
            raise ValueError(f'Unknown model: {model}')


class SimilarityLabelAssigner(BaseLabelAssigner):

    def __init__(self, model, scaler, feature_select, configs_path='../configs'):
        super().__init__(model, scaler, feature_select, configs_path)

    def _get_model_fn(self, model):
        # model is actually the metric
        if model in [
            'rmse', 'r2', 'mae', 'median_absolute_error', 'pearson', 'spearman', 'euclidean',
            'SID', 'SAM', 'entropy', 'NormXCorr', 'chebyshev'
        ]:
            return SimilarityLabelRegressor(model)
        else:
            raise ValueError(f'Unknown model: {model}')
