import pandas as pd
from veg_estimator.utils import file_exists
from veg_estimator.enviroutils import S2_band_wavelength_mapping

class DataLoader():

    def __init__(self):
        self.base_data_path = '/storage/brno3-cerit/home/tslaninakova/enviro-nn/data'
        
        self.valid_data_path = f'{self.base_data_path}/val_Lanzhot_interaction-image.csv'
        
        base_LUT_path = f'{self.base_data_path}/raw-input-data/Lanzhot/S2-lut'
        self.LUT_path = f'{base_LUT_path}/lut_Lanzhot_sun0_flat.dart'
        self.LUT_params_path = f'{base_LUT_path}/params_Lanzhot_sun0_flat.dart'

        for path in [self.LUT_path, self.LUT_params_path, self.valid_data_path]:
            assert file_exists(path), f'File {path} does not exist.'

        self.veg_parameters = ['Cab', 'Car', 'Cw']

    def load_valid_data(self):
        df = pd.read_csv(self.valid_data_path)
        df = df.rename(
            columns={
                'Cab [µg cm-2]': 'Cab',
                'Car [µg cm-2]': 'Car',
                'EWT [mg cm-2]': 'Cw'
            }
        )

        df['Cw'] /= 10_000

        valid_params = df.loc[:, self.veg_parameters]
        valid_df = df.loc[:, df.columns.str.startswith('S2')]
        assert valid_df.shape[1] == 9, 'Valid data must have 9 columns.'
        valid_df = valid_df.rename(columns=S2_band_wavelength_mapping())
        valid_df /= 10_000

        return valid_df, valid_params

    def load_LUT_data(self):
        s2_lut = pd.read_csv(self.LUT_path, sep=' ')
        s2_lut.columns = [round(float(c)*1000,2) for c in s2_lut.columns]
        s2_lut_params = pd.read_csv(self.LUT_params_path, sep=' ').loc[:, self.veg_parameters]
        assert s2_lut.shape[1] == 13, 'LUT must have 13 rows.'
        assert s2_lut.shape[0] == s2_lut_params.shape[0], 'LUT and LUT params must have the same number of rows.'
        #s2_lut.columns = round_to_two_decimals_if_needed(s2_lut.columns.to_series())
        return s2_lut, s2_lut_params