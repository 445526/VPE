FROM python:3.8

ADD requirements* /var/python-requirements/

RUN pip install -r /var/python-requirements/requirements.txt && pip install -r /var/python-requirements/requirements-ci.txt
