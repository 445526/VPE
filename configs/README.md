# Configuration files used for GridSearchCV
- most of them from `scikit-learn` version `1.2.2`
- `xgboost` version `1.7.6` (Gradient Boosting)
- `lightgbm` version `4.0.0` (LGBMRegressor)
- can be used with any type of regression task